# Project API REST

Project is using django-rest-framework

### Prerequisites

Install docker-compose

### Installing


Download app by git clone

```
git clone https://matdarg@bitbucket.org/matdarg/project-api-rest.git
```

Open settings.py file

```
project/settings.py
```

Configure email backend i.e.
```
EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
EMAIL_HOST = 'example.com'
EMAIL_HOST_USER = 'admin@example.com'
EMAIL_HOST_PASSWORD = 'password'
EMAIL_USE_TLS = True
EMAIL_PORT = 587
```

Run project:

```
docker-compose up
```

Migrate database django container:

```
docker-compose run django python manage.py migrate
```