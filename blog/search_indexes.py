import datetime
from haystack import indexes
from .models import Entry


class EntryIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='author')
    pub_date = indexes.CharField(model_attr='pub_date')
    title = indexes.CharField(model_attr='title')
    id = indexes.IntegerField(model_attr='id')


    def get_model(self):
        return Entry

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated"""
        return self.get_model().objects.filter(pub_date__lte=datetime.datetime.now())
