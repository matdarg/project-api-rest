from django.db import models
from django.utils import timezone
from django.conf import settings


class EntryManager(models.Manager):
    def active(self, *args, **kwargs):
        return super(EntryManager, self).filter(pub_date__lte=timezone.now())


class Entry(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    body = models.TextField()
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    modified = models.DateTimeField(auto_now=True, auto_now_add=False)
    pub_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    comments_count = models.IntegerField(default=0)

    objects = EntryManager()

    def __str__(self):
        """Title of entry is string representation of Entry objects"""
        return str(self.title)