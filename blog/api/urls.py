from django.urls import path, include
from rest_framework import routers

from .views import (
    EntryListAPIView,
    EntryDetailAPIView,
    EntryUpdateAPIView,
    EntryDeleteAPIView,
    EntryCreateAPIView,
    EntrySearchAPIView
)
router = routers.DefaultRouter()
router.register("search", EntrySearchAPIView, base_name="search")

urlpatterns = [
    path('', EntryListAPIView.as_view(), name='list'),
    path('<int:pk>/', EntryDetailAPIView.as_view(), name='detail'),
    path('create/', EntryCreateAPIView.as_view(), name='create'),
    path('<int:pk>/edit/', EntryUpdateAPIView.as_view(), name='update'),
    path('<int:pk>/delete/', EntryDeleteAPIView.as_view(), name='delete'),
    path('', include(router.urls))
]
