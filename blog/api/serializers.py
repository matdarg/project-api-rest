from rest_framework.serializers import ModelSerializer, SerializerMethodField
from drf_haystack.serializers import HaystackSerializer

from comment.models import Comment
from comment.api.serializers import CommentSerializer
from ..models import Entry
from ..search_indexes import EntryIndex


class EntryCreateUpdateSerializer(ModelSerializer):
    """serializer for creating and updating entires"""

    class Meta:
        model = Entry
        fields = [
            'title',
            'body',
            'pub_date',
        ]


class EntrySearchSerializer(HaystackSerializer):

    class Meta:
        index_classes = [EntryIndex]
        fields = [
            'author',
            'title',
            'pub_date',
            'id'
        ]


class EntryListSerializer(ModelSerializer):
    """serializer for get entries list"""

    author = SerializerMethodField()

    class Meta:
        model = Entry
        fields = [
            'id',
            'author',
            'title',
            'pub_date',
            'created',
            'modified',
            'pub_date',
            'comments_count',
        ]

    def get_author(self, obj):
        """method for author's SerializerMethodField"""
        return str(obj.author.username)


class EntryDetailSerializer(ModelSerializer):
    """serializer for get entry details"""
    author = SerializerMethodField()
    comments = SerializerMethodField()

    class Meta:
        model = Entry
        fields = [
            'id',
            'author',
            'title',
            'body',
            'pub_date',
            'created',
            'modified',
            'pub_date',
            'comments_count',
            'comments',
        ]

    def get_author(self, obj):
        """method for author's SerializerMethodField, returns username"""
        return str(obj.author.username)

    def get_comments(self, obj):
        """method for comments's SerializerMethodField, returns serialized comments for Entry"""
        comments_qs = Comment.objects.filter_by_instance(obj)
        comments = CommentSerializer(comments_qs, many=True).data
        return comments