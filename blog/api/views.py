from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    DestroyAPIView,
    CreateAPIView
)
from rest_framework.permissions import (
    IsAuthenticated,
    IsAuthenticatedOrReadOnly,
)
from drf_haystack.viewsets import HaystackViewSet
from drf_haystack.filters import HaystackAutocompleteFilter

from ..models import Entry

from .permissions import IsOwnerOrReadOnly
from .serializers import (
    EntryCreateUpdateSerializer,
    EntryDetailSerializer,
    EntryListSerializer,
    EntrySearchSerializer,
)


class EntryCreateAPIView(CreateAPIView):
    """Entry Create View for POST requests,
    only logged in user can create Entry"""

    queryset = Entry.objects.all()
    serializer_class = EntryCreateUpdateSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        """saving serializer with author as user from request"""
        serializer.save(author=self.request.user)

class EntrySearchAPIView(HaystackViewSet):
    """Entry Search View for GET requests to search entries"""
    serializer_class = EntrySearchSerializer
    index_models = [Entry]
    filter_backends = [HaystackAutocompleteFilter]


class EntryListAPIView(ListAPIView):
    """Entry List View for GET requests to get all publicated Entries"""

    serializer_class = EntryListSerializer
    queryset = Entry.objects.active()


class EntryDetailAPIView(RetrieveAPIView):
    """Entry Detail View for GET requests to retrieve publicated Entry"""

    queryset = Entry.objects.active()
    serializer_class = EntryDetailSerializer


class EntryUpdateAPIView(RetrieveUpdateAPIView):
    """Entry Update View for PUT, PATCH requests to update Entry
        only owner (author) can modify Entry"""

    queryset = Entry.objects.all()
    serializer_class = EntryCreateUpdateSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_update(self, serializer):
        """update serializer with author as user from request"""
        serializer.save(author=self.request.user)


class EntryDeleteAPIView(DestroyAPIView):
    """Entry Delete View for DELETE requests to remove Entry.
            Only owner (author) can delete Entry"""

    queryset = Entry.objects.all()
    serializer_class = EntryDetailSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
