from django.db import models
from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


def get_sentinel_user():
    return get_user_model().objects.get_or_create(username='deleted')[0]


class CommentManager(models.Manager):

    def filter_by_instance(self, instance):
        content_type = ContentType.objects.get_for_model(instance.__class__)
        obj_id = instance.id
        qs = super(CommentManager, self).filter(content_type=content_type, object_id= obj_id)
        return qs

    def create_by_model_type(self, model_type, pk, body, user):
        model_qs = ContentType.objects.filter(model=model_type)
        if model_qs.exists():
            SomeModel = model_qs.first().model_class()
            obj_qs = SomeModel.objects.filter(pk=pk)
            if obj_qs.exists() or obj_qs.count() == 1:
                instance = self.model()
                instance.body = body
                instance.user = user
                instance.content_type = model_qs.first()
                obj = obj_qs.first()
                instance.object_id = obj.id
                instance.save()
                return instance
        return None


class Comment(models.Model):
    """Comment model with generic foreign keys"""
    user = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.SET(get_sentinel_user))
    body = models.TextField()
    created = models.DateTimeField(auto_now=False, auto_now_add=True)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    objects = CommentManager()

    def __str__(self):
        return str(self.user.username)