from haystack import indexes
from .models import Comment


class CommentIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    user = indexes.CharField(model_attr='user')
    created = indexes.CharField(model_attr='created')
    body = indexes.CharField(model_attr='body')
    id = indexes.IntegerField(model_attr='id')

    def get_model(self):
        return Comment

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
