from django.db.models import F
from django.contrib.contenttypes.models import ContentType
from celery import shared_task


@shared_task
def increase_comments_count(model_type, pk):
    """celery's task for increase comments count in object"""
    model_qs = ContentType.objects.filter(model=model_type)
    SomeModel = model_qs.first().model_class()
    SomeModel.objects.filter(pk=pk).update(comments_count=F('comments_count')+1)
