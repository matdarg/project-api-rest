from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from rest_framework.serializers import (
    ModelSerializer,
    SerializerMethodField,
    ValidationError,
)
from drf_haystack.serializers import HaystackSerializer

from ..models import Comment
from ..tasks import increase_comments_count
from ..search_indexes import CommentIndex

User = get_user_model()


def create_comment_serializer(model_type='article', pk=None, user=None):
    """Method returns serializer class created from model_type, pk, user parameters
    to use with different ContentTypes
    """
    class CommentCreateSerializer(ModelSerializer):

        class Meta:
            model = Comment
            fields = [
                'body'
            ]

        def __init__(self, *args, **kwargs):
            self.model_type = model_type
            self.pk = pk
            super(CommentCreateSerializer, self).__init__(*args, **kwargs)

        def validate(self, data):
            """Method checks if provided model_type and pk are"""
            model_type = self.model_type
            model_qs = ContentType.objects.filter(model=model_type)
            if not model_qs.exists() or model_qs.count() != 1:
                raise ValidationError("This is not a valid content type")
            SomeModel = model_qs.first().model_class()
            obj_qs = SomeModel.objects.filter(pk=self.pk)
            if not obj_qs.exists() or obj_qs.count() != 1:
                raise ValidationError("This is not a valid pk for this content type")
            return data

        def create(self, validated_data):
            """method creates new comment in database for specified object.
            method send request to celery to increase comments_count in specified object
            """
            body = validated_data.get("body")
            if user:
                main_user = user
            else:
                main_user = User.objects.all().first()
            model_type = self.model_type
            pk = self.pk
            comment = Comment.objects.create_by_model_type(model_type, pk, body, main_user)
            increase_comments_count.delay(model_type, pk)
            return comment

    return CommentCreateSerializer


class CommentSerializer(ModelSerializer):

    user = SerializerMethodField()

    class Meta:
        model = Comment
        fields = [
            'id',
            'user',
            'body',
            'created',
        ]

    def get_user(self, obj):
        """get username of user object"""
        return str(obj.user.username)


class CommentDetailSerializer(ModelSerializer):
    """Comment Detail Serializer for retrieve details of comment"""

    class Meta:
        model = Comment
        fields = [
            'id',
            'user',
            'object_id',
            'body',
            'created',
        ]


class CommentSearchSerializer(HaystackSerializer):

    class Meta:
        index_classes = [CommentIndex]
        fields = [
            'user',
            'body',
            'created',
            'id'
        ]