from django.urls import path, include


from .views import (
    CommentListAPIView,
    CommentDetailAPIView,
    CommentCreateAPIView,
    CommentSearchAPIView
    )

from rest_framework import routers
router = routers.DefaultRouter()
router.register("search", CommentSearchAPIView, base_name="search")

urlpatterns = [
    path('', CommentListAPIView.as_view(), name='list'),
    path('create/', CommentCreateAPIView.as_view(), name='create'),
    path('<int:pk>/', CommentDetailAPIView.as_view(), name='detail'),
    path('', include(router.urls))
]
