from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    CreateAPIView
)
from rest_framework.permissions import IsAuthenticated

from drf_haystack.viewsets import HaystackViewSet
from drf_haystack.filters import HaystackAutocompleteFilter

from ..models import Comment
from .serializers import (
    CommentSerializer,
    CommentDetailSerializer,
    create_comment_serializer,
    CommentSearchSerializer,
)


class CommentCreateAPIView(CreateAPIView):
    queryset = Comment.objects.all()
    permission_classes = [IsAuthenticated]

    def get_serializer_class(self):
        model_type = self.request.GET.get("type", 'blog')
        pk = self.request.GET.get("pk")
        return create_comment_serializer(model_type=model_type, pk=pk, user=self.request.user)


class CommentListAPIView(ListAPIView):

    serializer_class = CommentSerializer
    queryset = Comment.objects.all()


class CommentDetailAPIView(RetrieveAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentDetailSerializer


class CommentSearchAPIView(HaystackViewSet):
    """Comment Search View for GET requests to search comments"""
    serializer_class = CommentSearchSerializer
    index_models = [Comment]
    filter_backends = [HaystackAutocompleteFilter]