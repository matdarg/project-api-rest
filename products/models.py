from django.db import models


class Product(models.Model):
    """
    Product model
    """
    name = models.CharField(max_length=255)
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00)

    def __str__(self):
        """
        string representation of Product object
        """
        return f'{self.name}, price: {self.price}'
