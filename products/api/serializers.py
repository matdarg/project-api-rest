from rest_framework.serializers import ModelSerializer
from ..models import Product


class ProductSerializer(ModelSerializer):
    """
    Product model serializer
    """

    class Meta:
        model = Product
        fields = '__all__'
