from rest_framework import viewsets
from rest_framework.permissions import IsAdminUser

from ..models import Product
from .serializers import (
    ProductSerializer,
)

class ProductViewSet(viewsets.ModelViewSet):
    """
    Products viewset which provides standard REST actions
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer

    def get_permissions(self):
        """
        Only admin users can create and edit products
        """
        if self.action in ['list', 'retrieve']:
            permission_classes = []
        else:
            permission_classes = [IsAdminUser]
        return [permission() for permission in permission_classes]