from django.urls import path, include
from rest_framework import routers

from .views import (
    ArticleListAPIView,
    ArticleDetailAPIView,
    ArticleUpdateAPIView,
    ArticleDeleteAPIView,
    ArticleCreateAPIView,
    ArticleSearchAPIView,
)

router = routers.DefaultRouter()
router.register("search", ArticleSearchAPIView, base_name="search")

app_name = 'articles'

urlpatterns = [
    path('', ArticleListAPIView.as_view(), name='list'),
    path('<int:pk>/', ArticleDetailAPIView.as_view(), name='detail'),
    path('create/', ArticleCreateAPIView.as_view(), name='create'),
    path('<int:pk>/edit/', ArticleUpdateAPIView.as_view(), name='update'),
    path('<int:pk>/delete/', ArticleDeleteAPIView.as_view(), name='delete'),
    path('', include(router.urls))
]
