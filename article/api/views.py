from rest_framework.generics import (
    ListAPIView,
    RetrieveAPIView,
    RetrieveUpdateAPIView,
    DestroyAPIView,
    CreateAPIView
)

from rest_framework.permissions import (
    IsAuthenticated,
    IsAuthenticatedOrReadOnly,
)
from drf_haystack.viewsets import HaystackViewSet
from drf_haystack.filters import HaystackAutocompleteFilter

from blog.api.permissions import IsOwnerOrReadOnly

from ..models import Article
from .serializers import (
    ArticleCreateUpdateSerializer,
    ArticleDetailSerializer,
    ArticleListSerializer,
    ArticleSearchSerializer
)


class ArticleCreateAPIView(CreateAPIView):
    """Article Create View for POST requests,
        only logged in user can create Article"""
    queryset = Article.objects.all()
    serializer_class = ArticleCreateUpdateSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

class ArticleSearchAPIView(HaystackViewSet):
    """Article Search View for GET requests to search articles"""
    serializer_class = ArticleSearchSerializer
    index_models = [Article]
    filter_backends = [HaystackAutocompleteFilter]

class ArticleListAPIView(ListAPIView):
    """Article List View for GET requests to get all publicated Articles"""
    serializer_class = ArticleListSerializer
    queryset = Article.objects.active()

class ArticleDetailAPIView(RetrieveAPIView):
    """Article Detail View for GET requests to retrieve publicated Article"""
    queryset = Article.objects.active()
    serializer_class = ArticleDetailSerializer


class ArticleUpdateAPIView(RetrieveUpdateAPIView):
    """Article Update View for PUT, PATCH requests to update Article.
            Only owner (author) can modify Entry"""
    queryset = Article.objects.all()
    serializer_class = ArticleCreateUpdateSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]

    def perform_update(self, serializer):
        serializer.save(author=self.request.user)


class ArticleDeleteAPIView(DestroyAPIView):
    """Article Delete View for DELETE requests to remove Article.
                Only owner (author) can delete Article"""
    queryset = Article.objects.all()
    serializer_class = ArticleDetailSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
