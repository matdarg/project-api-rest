from rest_framework.serializers import ModelSerializer, SerializerMethodField
from drf_haystack.serializers import HaystackSerializer

from comment.models import Comment
from comment.api.serializers import CommentSerializer

from ..models import Article
from ..search_indexes import ArticleIndex


class ArticleCreateUpdateSerializer(ModelSerializer):
    """serializer for creating and updating articles"""

    class Meta:
        model = Article
        fields = [
            'title',
            'body',
            'pub_date',
        ]


class ArticleSearchSerializer(HaystackSerializer):

    class Meta:
        index_classes = [ArticleIndex]
        fields = [
            'author',
            'title',
            'pub_date',
            'id',
            'autocomplete',
        ]


class ArticleListSerializer(ModelSerializer):
    """serializer for get articles list"""
    author = SerializerMethodField()

    class Meta:
        model = Article
        fields = [
            'id',
            'author',
            'title',
            'pub_date',
            'created',
            'modified',
            'pub_date',
            'comments_count',
        ]

    def get_author(self, obj):
        """method for author's SerializerMethodField"""
        return str(obj.author.username)


class ArticleDetailSerializer(ModelSerializer):
    """serializer for retrieve article's details"""

    author = SerializerMethodField()
    comments = SerializerMethodField()

    class Meta:
        model = Article
        fields = [
            'id',
            'author',
            'title',
            'body',
            'pub_date',
            'created',
            'modified',
            'pub_date',
            'comments_count',
            'comments',
        ]

    def get_author(self, obj):
        """method for author's SerializerMethodField"""
        return str(obj.author.username)

    def get_comments(self, obj):
        """method for comments's SerializerMethodField"""
        comments_qs = Comment.objects.filter_by_instance(obj)
        comments = CommentSerializer(comments_qs, many=True).data
        return comments

