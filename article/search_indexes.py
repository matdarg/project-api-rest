from django.utils import timezone
from haystack import indexes
from .models import Article


class ArticleIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='author')
    pub_date = indexes.CharField(model_attr='pub_date')
    title = indexes.CharField(model_attr='title')
    id = indexes.IntegerField(model_attr='id')
    autocomplete = indexes.EdgeNgramField()

    @staticmethod
    def prepare_autocomplete(obj):
        return " ".join((str(obj.author), obj.title))

    def get_model(self):
        return Article

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated"""
        return self.get_model().objects.filter(pub_date__lte=timezone.now())

