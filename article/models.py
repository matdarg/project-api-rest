from django.db import models
from django.utils import timezone
from django.conf import settings

class ArticleManager(models.Manager):
    """Article Manager with method to get only active (publicated) articles"""

    def active(self, *args, **kwargs):
        """method to get only publicated articles"""
        return super(ArticleManager, self).filter(pub_date__lte=timezone.now())


class Article(models.Model):
    """Article model"""
    author = models.ForeignKey(settings.AUTH_USER_MODEL, default=1, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    body = models.TextField()
    created = models.DateTimeField(auto_now=False, auto_now_add=True)
    modified = models.DateTimeField(auto_now=True, auto_now_add=False)
    pub_date = models.DateTimeField(auto_now=False, auto_now_add=False)
    comments_count = models.IntegerField(default=0)

    objects = ArticleManager()

    def __str__(self):
        """Title of article is string representation of Article objects"""
        return self.title