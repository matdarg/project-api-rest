from django.db import models
from django.conf import settings
from products.models import Product


class Order(models.Model):
    """
    Order model with fields: user (owner of order), full price, products
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='orders', on_delete=models.CASCADE)
    full_price = models.DecimalField(max_digits=10, decimal_places=2, default=0.00, editable=False)
    products = models.ManyToManyField(Product, through="OrderProduct")

    def __str__(self):
        """
        String representation of Order object
        """
        return f'Order with id {self.pk}'


class OrderProduct(models.Model):
    """
    Intermediate model associated with ManyToManyField in Order's field products
    Provides quantity of the product on order
    """
    order = models.ForeignKey(Order, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        """
        String representation of Order Product object
        """
        return str(self.product) + f' quantity: {self.quantity}'
