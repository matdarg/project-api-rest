from django.core import mail
from django.conf import settings
from django.db.models import Prefetch
from celery import shared_task
from .models import OrderProduct, Order


@shared_task
def update_full_price(order_id):
    """
    Celery task to update full price in order
    """
    order = Order.objects.get(id=order_id)
    ordered_products = OrderProduct.objects.select_related('order, product').filter(order=order)

    full_price = 0

    for ordered_product in ordered_products:
        full_price += ordered_product.product.price * ordered_product.quantity

    order.full_price = full_price
    order.save()


@shared_task
def send_email_with_order(order_id):
    """
    Celery task to send email with order
    """
    lookup = Prefetch('products', queryset=OrderProduct.objects.select_related('order', 'product'))
    order = Order.objects.select_related('user').prefetch_related(lookup).get(id=order_id)

    email = order.user.email

    message = 'Products: ' + ' ,'.join(str(product) for product in order.products.all()) + \
              f', Full price: {order.full_price}'

    mail.send_mail(
        'Your order',
        message,
        settings.EMAIL_HOST_USER,
        [email],
    )