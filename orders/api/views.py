from django.db.models import Prefetch

from rest_framework import generics, views
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from ..models import Order, OrderProduct
from ..tasks import send_email_with_order

from .serializers import (
    OrderAddUpdateProductSerializer,
    OrderDetailsSerializer,
    OrderListSerializer,
    OrderSendSerializer,
)
from .permissions import (
    IsOwner,
    ProductExistsOnOrder,
    UserHasEmail,
)


class OrderListAPIView(generics.ListCreateAPIView):
    """
    APIView which allows get user orders list and creating new order
    """
    serializer_class = OrderListSerializer

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user).values()

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class OrderDetailAPIView(generics.RetrieveAPIView):
    """
    APIView to show details of order
    """
    serializer_class = OrderDetailsSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        lookup = Prefetch('products', queryset=OrderProduct.objects.select_related('order', 'product'))
        return Order.objects.select_related('user').prefetch_related(lookup)


class OrderAddProductAPIView(generics.CreateAPIView):
    """
    APIView to create new product on order
    """
    serializer_class = OrderAddUpdateProductSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(order_id=self.kwargs['pk'])

    def get_permissions(self):
        """
        Prevent adding product which exists on order
        """
        if self.request.method == 'POST':
            self.permission_classes += [ProductExistsOnOrder]
        return super().get_permissions()


class OrderUpdateProductAPIView(generics.UpdateAPIView):
    """
    APIView to update quantity of ordered product
    """
    serializer_class = OrderAddUpdateProductSerializer
    permission_classes = [IsAuthenticated, IsOwner]

    def get_queryset(self):
        return Order.objects.filter(user=self.request.user)

    def perform_update(self, serializer):
        serializer.save(order_id=self.kwargs['pk'])


class OrderSendView(views.APIView):
    """
    APIView to send order to email
    """
    permission_classes = [IsAuthenticated, IsOwner, UserHasEmail]

    def get(self, request, pk=None, format=None):
        data = Order.objects.get(pk=pk)
        serializer = OrderSendSerializer(data, context={'request': request})
        return Response(serializer.data)

    def post(self, request, pk=None, format=None):
        send_email_with_order.delay(pk)
        return Response({'message': 'The order has been sent'})
