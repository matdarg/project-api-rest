from rest_framework.permissions import BasePermission
from ..models import OrderProduct


class IsOwner(BasePermission):
    """
    Permission class which check if request user is author of object
    """
    message = 'You must be the owner of this object.'

    def has_object_permission(self, request, view, obj):
        return obj.user == request.user


class ProductExistsOnOrder(BasePermission):
    """
    Permission class which prevent to create the same product on order
    """
    message = 'This object is already on order list, please update quantity instead of create similar product order'

    def has_permission(self, request, view):
        object_exists = OrderProduct.objects.filter(
            product__id=request.POST.get('product'),
            order_id=view.kwargs.get('pk')
        ).exists()
        return not object_exists


class UserHasEmail(BasePermission):
    """
    Permission class which prevent sending email to user without email address
    """
    message = 'User does not have email address. Please add email address to user account.'

    def has_permission(self, request, view):
        if not request.user.email:
            return False
        else:
            return True