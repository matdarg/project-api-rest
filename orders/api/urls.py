from django.urls import path
from .views import (
    OrderDetailAPIView,
    OrderAddProductAPIView,
    OrderListAPIView,
    OrderUpdateProductAPIView,
    OrderSendView,
)


app_name = 'orders'

urlpatterns = [
    path('', OrderListAPIView.as_view(), name='order_list'),
    path('<int:pk>/', OrderDetailAPIView.as_view(), name='order_details'),
    path('<int:pk>/add-product', OrderAddProductAPIView.as_view(), name='order_add_product'),
    path('<int:pk>/update-product-quantity',
         OrderUpdateProductAPIView.as_view(),
         name='order_update_product_quantity'),
    path('<int:pk>/send-order', OrderSendView.as_view(), name='send_order')
]
