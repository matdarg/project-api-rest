from rest_framework.serializers import (
    ModelSerializer, SerializerMethodField, ReadOnlyField, HyperlinkedIdentityField, ValidationError
)

from ..models import Order, OrderProduct
from ..tasks import update_full_price


class OrderProductSerializer(ModelSerializer):
    """
    Order Product serializer - used to show product properties (name, price, quantity) on order
    """
    product = ReadOnlyField(source='product.name')
    price = ReadOnlyField(source='product.price')

    class Meta:
        model = OrderProduct
        fields = [
            'product',
            'price',
            'quantity',
        ]

class OrderListSerializer(ModelSerializer):
    """
    Order list serializer - used to show all user orders without order details
    """
    full_price = ReadOnlyField()

    class Meta:
        model = Order
        fields = [
            'id',
            'full_price',
        ]



class OrderDetailsSerializer(ModelSerializer):
    """
    Order details serializer - used to show details of order - all products, order url management, full price
    """
    products = OrderProductSerializer(source='orderproduct_set', many=True)

    add_product_url = HyperlinkedIdentityField(
        view_name='orders:order_add_product',
    )
    update_product_url = HyperlinkedIdentityField(
        view_name='orders:order_update_product_quantity',
    )
    send_order_url = HyperlinkedIdentityField(
        view_name='orders:send_order'
    )

    class Meta:
        model = Order
        fields = [
            'id',
            'products',
            'full_price',
            'add_product_url',
            'update_product_url',
            'send_order_url',
        ]

    def get_products(self, obj):
        return OrderProductSerializer(obj.products, many=True).data


class OrderAddUpdateProductSerializer(ModelSerializer):
    """
    Serializer for adding and updating products on order list
    """

    class Meta:
        model = OrderProduct
        fields = [
            'product',
            'quantity',
        ]

    def create(self, validated_data):
        """Method for creating new product on order list"""
        instance = super(OrderAddUpdateProductSerializer, self).create(validated_data)
        update_full_price.delay(instance.order.id)
        return instance

    def update(self, instance, validated_data):
        """
        Method for update product quantity.
        If product does not exists on order - raise ValidationError,
        If sent quantity is 0 - product is removed from list
        After update quantity, full order price is updated by celery task
        """
        product = self.initial_data.get('product')
        quantity = int(self.initial_data.get('quantity', 0))
        objects = OrderProduct.objects.filter(order=instance, product_id=product)
        if not objects.count():
            raise ValidationError("Product not found on order.", code=404)
        if quantity == 0:
            objects.delete()
        else:
            objects.update(quantity=quantity)

        instance.__dict__.update(**validated_data)
        #
        update_full_price.delay(instance.id)
        return instance

class OrderSendSerializer(ModelSerializer):
    """
    Serializer for sending order - show order with email address to send email
    """
    order = SerializerMethodField()
    email = SerializerMethodField()

    class Meta:
        model = Order
        fields = [
            'order',
            'email',
        ]

    def get_order(self, obj):
        return OrderDetailsSerializer(obj, context={'request': self.context['request']}).data

    def get_email(self, obj):
        return obj.user.email