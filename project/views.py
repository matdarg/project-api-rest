from django.http import HttpResponse
from django.views.generic import TemplateView
import simplejson as json
from haystack.query import SearchQuerySet


def autocomplete(request):
    sqs = SearchQuerySet().autocomplete(content_auto=request.GET.get('q', ''))[:5]
    suggestions = [result.title for result in sqs]
    the_data = json.dumps({
        'results': suggestions
    })
    return HttpResponse(the_data, content_type='application/json')


class HomePage(TemplateView):
    template_name = 'search/search.html'